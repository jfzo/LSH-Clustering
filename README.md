# LSH-Clustering
This project will host the files that implement the algorithms appearing in the article:
"Hashing-based clustering for massive text data".

## Input format
The input vectors must be stored in plain-text files following Cluto's format, that is:
```
#Vectors #Dimensions #NonNullValues
V1_non_null_dimension1 V1_non_null_dimension1_value ...
V2_non_null_dimension1 V2_non_null_dimension1_value ...
...
```

For instance the vectors:
```
[1.10 0.00 0.09]
[0.00 1.02 0.55]
[0.00 0.00 0.01]
```

Would be stored in the input file as
```
3 3 5
1 1.10 3 0.09
2 1.02 3 0.55
3 0.01
```


## Output and Usage of the programs
After compiling, the programs available in the _bin_ directory are:

* jaccardest_mod1
* jaccardest_mod2
* cosest_mod1
* cosest_mod2

### About the procedure and the expected output

Each one of these programs takes an input file following the format described 
above, builds the signature for each input vector and then inserts it into the 
Sketch (array of hash tables).
Finally, from the Sketch, the programa re-constructs the similarity matrix and stores 
it into a ".simestimate" file. This file follows also Cluto's format, that is in
the first line the number of input instances is written and then in every line
the similarities between the current line instance and all the remaining ones 
(including itself) are written. The similarity matrix for the input data of the 
previous example is:
```
3
1.00 0.04 0.08
0.04 1.00 0.47
0.08 0.47 1.00
```


### About the parameters

Each of the programs above employs the six mandatory parameters that appear below:

|argument order |value                |Description                     |
|:--------|:--------------------------|:-------------------------------|
|1        |input-file                 |Input file with instance vectors|
|2        |label-file                 |File with one label for each instance in each line|
|3        |max-false-positive-error   |Used to estimate the optimal nr. of bands following Indyk's seminal work (__Not used to compute the estimate__, since these parameters are passed manually)|
|4        |similarity-threshold       |Used to estimate the optimal nr. of bands following Indyk's seminal work (__Not used to compute the estimate__, since these parameters are passed manually)|
|5        |number-of-rows-per-band    |Size of each band. It impacts on the False-positive rate of the near neighbor algorithm. |
|6        |number-of-bands            |Number of bands. It allows probability amplification.|

Parameters __3__ and __4__ are only employed for verification, since the near 
neighbor algorithm is built directly from the parameters __5__ and __6__ .

## Compilation instructions
First, locate into the root directory of the code. Listing the current directory will
show:
```
Makefile  README.md src
```
Then, create the '_bin_' directory into which the programs generated after the 
compilation process will be put.

In order to compile the implementation that uses binary input vectors and 
generates the estimated similarity 
matrix by using the __Mode-1__ estimate, execute the command:
```
$ make jaccardest_mod1
```
Otherwise, if you need to use the __Mode-1__ estimate, execute the command:
```
$ make jaccardest_mod2
```


A very similar scheme is employed to compile the implementation that employes real input vectors and generates the 
estimated similarity either by using the __Mode-1__ or __Mode-2__ estimates respectively:
```
$ make cosest_mod1
```

```
$ make cosest_mod2
```